package com.example.tictactoe

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private var playerOne = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        reset()
    }
    fun init(){
        button00.setOnClickListener(this)
        button01.setOnClickListener(this)
        button02.setOnClickListener(this)
        button10.setOnClickListener(this)
        button11.setOnClickListener(this)
        button12.setOnClickListener(this)
        button20.setOnClickListener(this)
        button21.setOnClickListener(this)
        button22.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.button00 ->{changerButton(button00)}
            R.id.button01 -> {changerButton(button01)}
            R.id.button02 -> {changerButton(button02)}
            R.id.button10 -> {changerButton(button10)}
            R.id.button11 ->{changerButton(button11)}
            R.id.button12 ->{changerButton(button12)}
            R.id.button20 -> {changerButton(button20)}
            R.id.button21 -> {changerButton(button21)}
            R.id.button22 -> {changerButton(button22)}
        }
    }

    private fun changerButton(button:Button){
        if (playerOne){
            button.text = "X"
        }else{
            button.text = "O"
        }
        button.isClickable = false
        playerOne = !playerOne
        winnerCheck()
    }

    private fun winnerCheck(){
        if (button00.text.toString().isNotEmpty() && button00.text.toString() == button02.text.toString() && button01.text.toString()== button02.text.toString()){
            Toast.makeText(this,"Winner is ${button00.text.toString()}", Toast.LENGTH_SHORT).show()
            finishGame()

        }else if(button10.text.toString().isNotEmpty() && button10.text.toString() == button11.text.toString() && button11.text.toString() == button12.text.toString()){
            Toast.makeText(this,"Winner is ${button10.text.toString()}", Toast.LENGTH_SHORT).show()
finishGame()
        }else if (button20.text.toString().isNotEmpty() && button20.text.toString() == button21.text.toString() && button21.text.toString() == button22.text.toString()){
            Toast.makeText(this,"Winner is ${button20.text.toString()}", Toast.LENGTH_SHORT).show()
            finishGame()
        }else if(button00.text.toString().isNotEmpty() && button00.text.toString() == button10.text.toString() && button10.text.toString() == button20.text.toString()){
            Toast.makeText(this,"Winner is ${button00.text.toString()}", Toast.LENGTH_SHORT).show()
            finishGame()
        }else if (button01.text.toString().isNotEmpty() && button01.text.toString() == button11.text.toString() && button11.text.toString() == button21.text.toString()){
            Toast.makeText(this,"Winner is ${button01.text.toString()}", Toast.LENGTH_SHORT).show()
            finishGame()
        }else if(button02.text.toString().isNotEmpty() && button02.text.toString() == button12.text.toString() && button12.text.toString() == button22.text.toString()){
            Toast.makeText(this,"Winner is ${button02.text.toString()}", Toast.LENGTH_SHORT).show()
            finishGame()
        }else if (button00.text.toString().isNotEmpty() && button00.text.toString() == button11.text.toString() && button11.text.toString() == button22.text.toString()){
            Toast.makeText(this,"Winner is ${button00.text.toString()}", Toast.LENGTH_SHORT).show()
            finishGame()
        }else if (button02.text.toString().isNotEmpty() && button02.text.toString() == button11.text.toString() && button11.text.toString() == button20.text.toString()){
            Toast.makeText(this,"Winner is ${button02.text.toString()}", Toast.LENGTH_SHORT).show()
            finishGame()
        }
    }
    private fun showToast(message:String){
        Toast.makeText(this,"Winner is $message", Toast.LENGTH_SHORT).show()
    }
    private fun reset(){
        tryAgainButton.setOnClickListener{
            button00.text = ""
            button01.text = ""
            button02.text = ""
            button10.text = ""
            button11.text = ""
            button12.text = ""
            button20.text = ""
            button21.text = ""
            button22.text = ""
            button00.isClickable = true
            button01.isClickable = true
            button02.isClickable = true
            button10.isClickable = true
            button12.isClickable = true
            button11.isClickable = true
            button20.isClickable = true
            button21.isClickable = true
            button22.isClickable = true
            playerOne = true
        }
    }
    private fun finishGame(){
        button00.isClickable = false
        button01.isClickable = false
        button02.isClickable = false
        button10.isClickable = false
        button12.isClickable = false
        button11.isClickable = false
        button20.isClickable = false
        button21.isClickable = false
        button22.isClickable = false

    }
private fun draw(){
    if(button00.text.toString().isNotEmpty() && button01.text.toString().isNotEmpty() && button02.text.toString().isNotEmpty() && button10.text.toString().isNotEmpty() && button11.text.toString().isNotEmpty() && button12.text.toString().isNotEmpty() && button20.text.toString().isNotEmpty() && button21.text.toString().isNotEmpty() && button22.text.toString().isNotEmpty() ){
        Toast.makeText(this, "Draw", Toast.LENGTH_SHORT)
    }
}
}